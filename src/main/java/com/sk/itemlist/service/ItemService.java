package com.sk.itemlist.service;

import com.sk.itemlist.domain.Item;

/**
 * Created by sk on 29.01.2016.
 */

public interface ItemService extends GenericService<Item, Long> {
}