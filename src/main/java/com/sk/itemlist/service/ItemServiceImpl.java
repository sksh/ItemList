package com.sk.itemlist.service;

import com.sk.itemlist.dao.ItemDao;
import com.sk.itemlist.domain.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sk on 29.01.2016.
 */

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao dao;

    @Override
    public void add(Item entity) {
        dao.add(entity);
    }

    @Override
    public void update(Item entity) {
        dao.update(entity);
    }

    @Override
    public void remove(Item entity) {
        dao.remove(entity);
    }

    @Override
    public List<Item> listAll() {
        return dao.listAll();
    }

    @Override
    public Item findById(long id) {
        return dao.findById(id);
    }

    @Override
    public Item findByKey(Long key) {
        return dao.findByKey(key);
    }
}