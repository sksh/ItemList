package com.sk.itemlist.service;

import com.sk.itemlist.domain.Category;

/**
 * Created by sk on 29.01.2016.
 */
public interface CategoryService extends GenericService<Category, String> {
}
