package com.sk.itemlist.service;

import java.util.List;

/**
 * Created by sk on 29.01.2016.
 */
public interface GenericService<T, K> {
    void add(T entity);

    void update(T entity);

    void remove(T entity);

    List<T> listAll();

    T findById(long id);

    T findByKey(K key);
}
