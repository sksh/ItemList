package com.sk.itemlist.service;

import com.sk.itemlist.dao.CategoryDao;
import com.sk.itemlist.domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sk on 29.01.2016.
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao dao;

    @Override
    public void add(Category entity) {
        dao.add(entity);
    }

    @Override
    public void update(Category entity) {
        dao.update(entity);
    }

    @Override
    public void remove(Category entity) {
        dao.remove(entity);
    }

    @Override
    public List<Category> listAll() {
        return dao.listAll();
    }

    @Override
    public Category findById(long id) {
        return dao.findById(id);
    }

    @Override
    public Category findByKey(String key) {
        return dao.findByKey(key);
    }
}
