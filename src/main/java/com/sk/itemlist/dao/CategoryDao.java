package com.sk.itemlist.dao;

import com.sk.itemlist.domain.Category;

/**
 * Created by sk on 27.01.2016.
 */
public interface CategoryDao extends GenericDao<Category, String> {
}
