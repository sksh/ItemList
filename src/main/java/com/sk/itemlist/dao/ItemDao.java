package com.sk.itemlist.dao;

import com.sk.itemlist.domain.Item;


/**
 * Created by sk on 26.01.2016.
 */
public interface ItemDao extends GenericDao<Item, Long> {
}