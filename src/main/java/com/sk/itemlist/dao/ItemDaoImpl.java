package com.sk.itemlist.dao;

import com.sk.itemlist.domain.Item;
import org.springframework.stereotype.Repository;


/**
 * Created by sk on 26.01.2016.
 */
@Repository("itemDao")
public class ItemDaoImpl extends HibernateDao<Item, Long> implements ItemDao {
}