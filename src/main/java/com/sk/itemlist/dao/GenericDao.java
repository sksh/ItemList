package com.sk.itemlist.dao;

import java.util.List;

/**
 * Created by sk on 25.01.2016.
 */
public interface GenericDao<T, K> {
    void add(T entity);

    void update(T entity);

    void remove(T entity);

    List<T> listAll();

    T findById(long id);

    T findByKey(K key);
}