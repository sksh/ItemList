package com.sk.itemlist.dao;

import com.sk.itemlist.domain.Category;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by sk on 27.01.2016.
 */
@Repository("categoryDao")
public class CategoryDaoImpl extends HibernateDao<Category, String> implements CategoryDao {
    @Override
    public Category findByKey(String key) {
        return (Category) currentSession()
                .createCriteria(Category.class)
                .add(Restrictions.eq("name", key))
                .uniqueResult();
    }
}