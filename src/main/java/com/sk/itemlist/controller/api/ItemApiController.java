package com.sk.itemlist.controller.api;

import com.sk.itemlist.domain.Item;
import com.sk.itemlist.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by sk on 25.01.2016.
 */
@RestController
@RequestMapping(value = "/api/item")
public class ItemApiController {

    @Autowired
    private ItemService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Item>> listAll() {
        return new ResponseEntity<>(service.listAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Item> getById(@PathVariable(value = "id") long id) {
        Item item = service.findById(id);

        if (item != null) {
            return new ResponseEntity<>(item, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Item> createRecord(@RequestBody Item item) {
        service.add(item);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateRecord(@PathVariable(value = "id") long id, @RequestBody Item item) {
        if (service.findById(id) != null) {
            service.update(item);

            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteRecord(@PathVariable(value = "id") long id) {
        Item itemById = service.findById(id);

        if (itemById != null) {
            service.remove(itemById);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}