package com.sk.itemlist.controller.api;

import com.sk.itemlist.domain.Category;
import com.sk.itemlist.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by sk on 27.01.2016.
 */
@RestController
@RequestMapping("/api/category")
public class CategoryApiController {

    @Autowired
    private CategoryService service;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Category>> listAll() {
        return new ResponseEntity<>(service.listAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> getById(@PathVariable(value = "id") long id) {
        Category category = service.findById(id);

        if (category != null) {
            return new ResponseEntity<>(category, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeById(@PathVariable(value = "id") long id) {
        Category category = service.findById(id);

        if (category != null) {
            service.remove(category);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}