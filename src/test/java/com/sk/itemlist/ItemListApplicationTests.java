package com.sk.itemlist;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ItemListApplication.class)
@WebAppConfiguration
@ComponentScan(value = "com.sk.itemlist")
public class ItemListApplicationTests {

    @Test
    public void contextLoads() {
    }
}